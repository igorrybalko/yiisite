<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Categories;
use yii\db\Query;
use yii\data\Pagination;

class CategoriesController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $categories = Categories::find()->all();

        return $this->render('index', ['categories' => $categories]);
    }

    public function actionCategory()
    {
        $slug = Yii::$app->request->getPathInfo();

        $query = new Query();
        $select = ['a.title', 'a.slug', 'a.id', 'a.content', 'a.cat_id', 'a.img', 'a.createdate', 'c.slug as cat_slug', 'c.title as cat_title'];
        $result = $query->
            select($select)->
            from('articles a')->
            innerJoin('categories c', 'c.id = a.cat_id')->
            where([
                'a.published' => 1,
                'c.slug' => $slug
            ]);

        // делаем копию выборки
        $countQuery = clone $result;

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 2]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $result->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('category', ['category' => $models, 'pages' => $pages]);
    }

}