<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Article;
use frontend\models\Categories;

class ArticleController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $slug = Yii::$app->request->get('slug');
        $category = $this->_getCategory();
        $article = Article::find()->where(['slug' => $slug, 'published' => 1])->one();

        return $this->render('index', ['article' => $article, 'category' => $category]);
    }

    private function _getCategory()
    {
        $path = explode('/',  Yii::$app->request->pathInfo);
        $category = Categories::find()->select(['title', 'slug'])->where(['slug' => $path[0]])->asArray()->one();
        return $category;

    }

}
