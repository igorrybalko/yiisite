<?php

namespace frontend\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property string $title
 * @property string $createdate
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createdate'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'createdate' => Yii::t('app', 'Createdate'),
        ];
    }

    public static function getItems($id)
    {
        $query = new Query();
        $select = ['i.title', 'i.url', 'i.id'];
        $result = $query->
        select($select)->
        from('menus m')->
        innerJoin('menuitems i', 'm.id = i.menuid')->
        where([
            'i.published' => 1,
            'm.id' => $id
        ])->orderBy('i.order')->
        all();

        return $result;
    }
}
