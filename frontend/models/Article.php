<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property integer $cat_id
 * @property string $img
 * @property integer $published
 * @property string $createdate
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'content', 'cat_id', 'img'], 'required'],
            [['content'], 'string'],
            [['cat_id', 'published'], 'integer'],
            [['createdate'], 'safe'],
            [['title', 'slug', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'content' => Yii::t('app', 'Content'),
            'cat_id' => Yii::t('app', 'Cat ID'),
            'img' => Yii::t('app', 'Img'),
            'published' => Yii::t('app', 'Published'),
            'createdate' => Yii::t('app', 'Createdate'),
        ];
    }
}
