declare var jQuery:any;

export default class jsPlagins {
    
    constructor(){
        (function($){

            /*tabs*/
            $.fn.simpleTabs = function (options:Object) {

                var settings = $.extend({
                    'title' : '.nametab',
                    'content': '.contenttab',
                    'cb': '',
                    'activeHead': 'actheadtab',
                    'activeContent': 'activetab'
                }, options);

                var nametab = $(this).find(settings.title), /*селектор имен табов*/
                    contenttab = $(this).find(settings.content),/*селектор содержимого табов*/
                    tabsBlock = this;
                nametab.on('click', function () {
                    var activeClass = $(this).hasClass(settings.activeHead);/*является ли имя таба активным?*/
                    if (!activeClass) {
                        var ind = $(this).index();
                        $(tabsBlock).find('.' + settings.activeHead).removeClass(settings.activeHead);
                        $(this).addClass(settings.activeHead);
                        $(tabsBlock).find('.' + settings.activeContent).removeClass(settings.activeContent);
                        contenttab.eq(ind).addClass(settings.activeContent);
                        if (settings.cb) {
                            settings.cb();
                        }
                    }
                });
            };

            /*accordion*/
            $.fn.simpleAccordion = function(options:Object){

                var settings = $.extend({
                    'title' : '.title-acc',
                    'content': '.content-acc',
                    'cb': '',
                    'speed': 400
                }, options);

                var acctitle = $(this).find(settings.title);

                acctitle.on('click', function(){
                    if(!$(this).next().is(':visible')) {
                        $(settings.content).slideUp(settings.speed);
                        $(settings.title).removeClass('active');
                    }
                    $(this).next().stop().slideToggle(settings.speed);
                    $(this).toggleClass('active');
                    if(settings.cb){
                        settings.cb();
                    }
                });
            };

        })(jQuery);
    }
    
}