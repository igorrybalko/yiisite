<?php
/* @var $this yii\web\View */
$this->title = $article['title'];
$this->params['breadcrumbs'][] = ['label' => $category['title'], 'url' => ['/' . $category['slug']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?=$this->title?></h1>
<div class="date">
    <?=$article['createdate']?>
</div>
<div class="pre-img">
</div>
<div class="content">
    <?=$article['content']?>
</div>