<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\Menu;

AppAsset::register($this);

require_once (__DIR__ . '/../ink/layout-header.php');?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= $bodyClass; ?>">
<?php $this->beginBody() ?>
<header class="header">
    <div class="top-header">
        <div class="container">
            <div class="logo">
                <a href="/"></a>
            </div>
        </div>
    </div>
    
    <div class="top-menu">
        <div class="container">
            <?=Menu::render(1);?>
        </div>
    </div>
    
</header>
<div class="wrap">

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n", // template for all links
            'activeItemTemplate' => "<li class='active breadcrumb-item'>{link}</li>\n",
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?><?php echo $testSetting;?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
