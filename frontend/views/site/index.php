<?php

/* @var $this yii\web\View */

$this->title = 'Radio hobby';
?>
<div class="site-index">

   <div class="container">
       <div class="row">
           <div class="col">
               <div class="test-slider">
                   <div class="tsitem">
                       <img src="/images/filemanager/slide1.jpg" />
                   </div>
                   <div class="tsitem">
                       <img src="/images/filemanager/slide2.jpg" />
                   </div>
                   <div class="tsitem">
                       <img src="/images/filemanager/slide3.jpg" />
                   </div>
                   <div class="tsitem">
                       <img src="/images/filemanager/slide4.jpg" />
                   </div>
               </div>
           </div>
       </div>
       <div class="row">
           <div class="col">
               <div class="some-block">
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur enim nemo officia voluptas!
                       Asperiores consequuntur deserunt illo magnam mollitia necessitatibus nesciunt perspiciatis quaerat
                       quis soluta! Eum exercitationem harum neque quos.</p>
                   <a class="btn btn-default" href="/">test button</a>
               </div>
           </div>
       </div>
   </div>
</div>