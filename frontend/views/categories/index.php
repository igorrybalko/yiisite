<?php
/* @var $this yii\web\View */

?>
<h1>categories/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>
<ul>
    <?php foreach ($categories as $category){ ?>
        <li><a href="<?=$category->slug?>"><?php echo $category->title; ?></a></li>
    <?php } ?>
</ul>

