<?php
use yii\widgets\LinkPager;
$this->title = $category[0]['cat_title'];
$this->params['breadcrumbs'][] = $this->title;
foreach ($category as $item){ ?>

    <div><a href="<?=$item['cat_slug'] . '/' . $item['slug'];?>"><?=$item['title'];?></a></div>

<?php }

echo LinkPager::widget([
    'pagination' => $pages,
]);