<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property string $title
 * @property string $createdate
 */
class Menus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createdate'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'createdate' => Yii::t('app', 'Createdate'),
        ];
    }
}
