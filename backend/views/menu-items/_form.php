<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

require_once (__DIR__ . '/../ink/inputDataParams.php');

/* @var $this yii\web\View */
/* @var $model backend\models\MenuItems */
/* @var $form yii\widgets\ActiveForm */
/* @var $dateInputParams inputDataParams.php */
?>

<div class="menu-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'menuid')->dropDownList($menuIds) ?>

    <?= $form->field($model, 'order')->input('number') ?>

    <?= $form->field($model, 'published')->checkbox() ?>

    <?= $form->field($model, 'createdate')->textInput($dateInputParams) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
