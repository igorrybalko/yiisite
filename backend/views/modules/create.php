<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Modules */

$this->title = Yii::t('app', 'Create Module ' . $moduleInfo->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'moduleFields' => $moduleInfo->fields,
        'type' => $moduleInfo->type
    ]) ?>

</div>
