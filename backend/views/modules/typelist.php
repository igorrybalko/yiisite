<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Modules */

$this->title = 'Modules type list';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <ul>
    <?php foreach ($modulesInfo as $module){?>

        <li><a href="<?= Url::to(['modules/create', 'type' => $module->type]);?>"><?=$module->name?></a></li>

    <?php } ?>
    </ul>
</div>
