<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$content = json_decode($model->content);

require_once (__DIR__ . '/../ink/inputDataParams.php');

/* @var $this yii\web\View */
/* @var $model backend\models\Modules */
/* @var $form yii\widgets\ActiveForm */
/* @var $dateInputParams inputDataParams.php */
?>

<div class="module-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->hiddenInput()->label('') ?>
    
    <?php foreach ($moduleFields as $field){
        $label = isset($field->label) ? $field->label : $field->name;
        $value = isset($content->fields->{$field->name}) ? $content->fields->{$field->name} : '';
        ?>

        <div class="form-group">
            <?php switch ($field->type) {
                case 'text':
                    echo '<label>' . $label . '</label>' .
                        '<input value="' . $value . '" type="text" class="form-control module-input" name="' . $field->name . '" />';
                    break;
                case 'number':

                    echo '<label>' . $label . '</label>' .
                        '<input value="' . $value . '" type="number" class="form-control module-input" name="' . $field->name . '" />';
                    break;
                case 'textarea':
                    echo '<label>' . $label . '</label>' .
                        '<textarea class="form-control module-input" name="' . $field->name . '">'
                            . $value
                            . '</textarea>';
                    break;
                case 'editor':
                    echo '<label>' . $label . '</label>' .
                        '<textarea class="editor form-control module-input" name="' . $field->name . '">'
                        . $value
                        . '</textarea>';
                    break;
            }

                ?>
        </div>

    <?php } ?>

    <?= $form->field($model, 'type')->hiddenInput(['value' => $type])->label('') ?>

    <?= $form->field($model, 'createdata')->textInput($dateInputParams) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJsFile( '/js/tinymce.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);