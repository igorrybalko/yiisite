<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Menus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Menus',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="menus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
