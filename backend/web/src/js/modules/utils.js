($=>{

    $(()=>{

        $('.module-form #w0').on('submit', function (e) {
            
            let customFieldData = {};
                customFieldData.fields = {};

            $('.module-input').each(function () {

                let value = $(this).val();
                    name = $(this).attr('name');

                customFieldData.fields[name] = value;

            });

            $('#modules-content').val(JSON.stringify(customFieldData));

        });

        console.log($('.editor').length);

        if($('.editor').length){
            console.log('test');

            tinymce.init({ 
                selector:'.editor',
                plugins: 'advlist autolink lists link charmap print preview anchor searchreplace visualblocks code fullscreen insertdatetime media table contextmenu paste image',
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
            });
        }

    });

})(jQuery);